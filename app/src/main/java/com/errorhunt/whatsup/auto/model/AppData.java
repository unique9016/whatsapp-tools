package com.errorhunt.whatsup.auto.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppData {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("banner_id")
    @Expose
    private String bannerid;
    @SerializedName("show_banner")
    @Expose
    private Integer banner;
    @SerializedName("native_id")
    @Expose
    private String nativeid;
    @SerializedName("show_native")
    @Expose
    private Integer _native;
    @SerializedName("interstitial_id")
    @Expose
    private String interstitialid;
    @SerializedName("show_interstitial")
    @Expose
    private Integer interstitial;
    @SerializedName("open_app_id")
    @Expose
    private String openadid;
    @SerializedName("show_open_app")
    @Expose
    private Integer openad;
    @SerializedName("rewardadid")
    @Expose
    private String rewardadid;
    @SerializedName("rewardad")
    @Expose
    private Integer rewardad;
    @SerializedName("adtype")
    @Expose
    private String adtype;
    @SerializedName("is_show_ads")
    @Expose
    private Integer isAdEnable;
    @SerializedName("in_appreview")
    @Expose
    private Integer inAppreview;
    @SerializedName("review")
    @Expose
    private Integer review;
    @SerializedName("isactive")
    @Expose
    private Integer isactive;
    @SerializedName("interstitial_count")
    @Expose
    private Integer ads_per_click;
    @SerializedName("show_interstitial_exit")
    @Expose
    private Integer exit_ad_enable;
    @SerializedName("ads_per_seassion")
    @Expose
    private Integer ads_per_session;
    @SerializedName("app_open_count")
    @Expose
    private Integer app_open_count;
    @SerializedName("show_open_app_splash")
    @Expose
    private Integer is_splash_on;
    @SerializedName("splash_time")
    @Expose
    private Integer splash_time;

    @SerializedName("LogoLink")
    @Expose
    private String LogoLink;
    @SerializedName("LogoLink_2")
    @Expose
    private String LogoLink_2;
    @SerializedName("LeaguesLink")
    @Expose
    private String LeaguesLink;
    @SerializedName("getPlayer")
    @Expose
    private String getPlayer;
    @SerializedName("getMatch")
    @Expose
    private String getMatch;
    @SerializedName("getLeagueName")
    @Expose
    private String getLeagueName;
    @SerializedName("bannerDataFlag")
    @Expose
    private String bannerDataFlag;
    @SerializedName("isShowHomeBanner")
    @Expose
    private Integer isShowHomeBanner;
    @SerializedName("reviewCount")
    @Expose
    private Integer reviewCount;
    @SerializedName("reviewPopUpCount")
    @Expose
    private Integer reviewPopUpCount;

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public Integer getReviewPopUpCount() {
        return reviewPopUpCount;
    }

    public void setReviewPopUpCount(Integer reviewPopUpCount) {
        this.reviewPopUpCount = reviewPopUpCount;
    }

    public Integer getIs_splash_on() {
        if (is_splash_on == null) {
            return 1;
        }
        return is_splash_on;
    }

    public void setIs_splash_on(Integer is_splash_on) {
        this.is_splash_on = is_splash_on;
    }

    public Integer getSplash_time() {
        if (splash_time == null) {
            return 10;
        }
        return splash_time;
    }

    public void setSplash_time(Integer splash_time) {
        this.splash_time = splash_time;
    }

    public Integer get_native() {
        return _native;
    }

    public void set_native(Integer _native) {
        this._native = _native;
    }

    public Integer getExit_ad_enable() {
        return exit_ad_enable;
    }

    public void setExit_ad_enable(Integer exit_ad_enable) {
        this.exit_ad_enable = exit_ad_enable;
    }

    public Integer getAds_per_session() {
        return ads_per_session;
    }

    public void setAds_per_session(Integer ads_per_session) {
        this.ads_per_session = ads_per_session;
    }

    public Integer getAds_per_click() {
        return ads_per_click;
    }

    public void setAds_per_click(Integer ads_per_click) {
        this.ads_per_click = ads_per_click;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBannerid() {
        return bannerid;
    }

    public void setBannerid(String bannerid) {
        this.bannerid = bannerid;
    }

    public Integer getBanner() {
        return banner;
    }

    public void setBanner(Integer banner) {
        this.banner = banner;
    }

    public String getNativeid() {
        return nativeid;
    }

    public void setNativeid(String nativeid) {
        this.nativeid = nativeid;
    }

    public Integer getNative() {
        return _native;
    }

    public void setNative(Integer _native) {
        this._native = _native;
    }

    public String getInterstitialid() {
        return interstitialid;
    }

    public void setInterstitialid(String interstitialid) {
        this.interstitialid = interstitialid;
    }

    public Integer getInterstitial() {
        return interstitial;
    }

    public void setInterstitial(Integer interstitial) {
        this.interstitial = interstitial;
    }

    public String getOpenadid() {
        return openadid;
    }

    public void setOpenadid(String openadid) {
        this.openadid = openadid;
    }

    public Integer getOpenad() {
        return openad;
    }

    public void setOpenad(Integer openad) {
        this.openad = openad;
    }

    public String getRewardadid() {
        return rewardadid;
    }

    public void setRewardadid(String rewardadid) {
        this.rewardadid = rewardadid;
    }

    public Integer getRewardad() {
        return rewardad;
    }

    public void setRewardad(Integer rewardad) {
        this.rewardad = rewardad;
    }

    public String getAdtype() {
        return adtype;
    }

    public void setAdtype(String adtype) {
        this.adtype = adtype;
    }

    public Integer getIsAdEnable() {
        return isAdEnable;
    }

    public void setIsAdEnable(Integer isAdEnable) {
        this.isAdEnable = isAdEnable;
    }

    public Integer getInAppreview() {
        return inAppreview;
    }

    public void setInAppreview(Integer inAppreview) {
        this.inAppreview = inAppreview;
    }

    public Integer getReview() {
        return review;
    }

    public void setReview(Integer review) {
        this.review = review;
    }

    public Integer getIsactive() {
        return isactive;
    }

    public void setIsactive(Integer isactive) {
        this.isactive = isactive;
    }


    public String getLogoLink() {
        return LogoLink;
    }

    public void setLogoLink(String logoLink) {
        LogoLink = logoLink;
    }

    public String getLogoLink_2() {
        return LogoLink_2;
    }

    public void setLogoLink_2(String logoLink_2) {
        LogoLink_2 = logoLink_2;
    }

    public String getLeaguesLink() {
        return LeaguesLink;
    }

    public void setLeaguesLink(String leaguesLink) {
        LeaguesLink = leaguesLink;
    }

    public String getGetPlayer() {
        return getPlayer;
    }

    public void setGetPlayer(String getPlayer) {
        this.getPlayer = getPlayer;
    }

    public String getGetMatch() {
        return getMatch;
    }

    public void setGetMatch(String getMatch) {
        this.getMatch = getMatch;
    }

    public String getGetLeagueName() {
        return getLeagueName;
    }

    public void setGetLeagueName(String getLeagueName) {
        this.getLeagueName = getLeagueName;
    }

    public Integer getIsShowHomeBanner() {
        return isShowHomeBanner;
    }

    public void setIsShowHomeBanner(Integer isShowHomeBanner) {
        this.isShowHomeBanner = isShowHomeBanner;
    }

    public String getBannerDataFlag() {
        return bannerDataFlag;
    }

    public void setBannerDataFlag(String bannerDataFlag) {
        this.bannerDataFlag = bannerDataFlag;
    }
}
