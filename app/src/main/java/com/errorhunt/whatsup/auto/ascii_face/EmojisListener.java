package com.errorhunt.whatsup.auto.ascii_face;

public interface EmojisListener {
    void onWpShare(String emojiUnicode);
    void onShare(String emojiUnicode);
    void onCopy(String emojiUnicode);
}
