package com.errorhunt.whatsup.auto.screens;

import static com.errorhunt.whatsup.auto.MyApplication.appOpenAd;
import static com.errorhunt.whatsup.auto.MyApplication.isShowingAd;
import static com.errorhunt.whatsup.auto.screens.ActMain.PERMISSIONS;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.errorhunt.whatsup.auto.MyApplication;
import com.errorhunt.whatsup.auto.R;
import com.errorhunt.whatsup.auto.Utils.Common;
import com.errorhunt.whatsup.auto.Utils.OpenAppAds;
import com.errorhunt.whatsup.auto.Utils.UserHelper;
import com.errorhunt.whatsup.auto.Utils.Utility;
import com.errorhunt.whatsup.auto.Utils.notifier.EventNotifier;
import com.errorhunt.whatsup.auto.Utils.notifier.EventState;
import com.errorhunt.whatsup.auto.Utils.notifier.IEventListener;
import com.errorhunt.whatsup.auto.Utils.notifier.NotifierFactory;
import com.errorhunt.whatsup.auto.databinding.ActStartBinding;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.FullScreenContentCallback;

import java.io.File;

public class ActStart extends BaseAct implements IEventListener {
    ActStartBinding binding;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            eventState = EventState.EVENT_PROCESSED;
            runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, ActStart.this, 1), 500));
        }

        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActStartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        registerAdsListener();

        binding.ivHrzBanner.setImageBitmap(Utility.decodeSampledBitmapFromResource(getResources(), R.drawable.e_hrz_banner, 500, 500));

        UserHelper.setUserInSplashIntro(false);
        if (appOpenAd != null && UserHelper.isNetworkConnected(this) && UserHelper.getOpenAdsSplash() == 1) {
            FullScreenContentCallback callback =
                    new FullScreenContentCallback() {
                        @Override
                        public void onAdDismissedFullScreenContent() {
                            UserHelper.isShowingFullScreenAd = false;
                            isShowingAd = false;
                            appOpenAd = null;
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad was dismissed.");
                            if (UserHelper.getShowAppOpen() == 1 && UserHelper.getIsAdEnable() == 1) {
                                OpenAppAds appLifecycleObserver = new OpenAppAds(MyApplication.getInstance());
                                ProcessLifecycleOwner.get().getLifecycle().addObserver(appLifecycleObserver);
                            }
                        }

                        @Override
                        public void onAdShowedFullScreenContent() {
                            UserHelper.isShowingFullScreenAd = true;
                            isShowingAd = true;
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad was shown.");
                        }

                        @Override
                        public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad failed to show.");
                            isShowingAd = true;
                            if (UserHelper.getShowAppOpen() == 1 && UserHelper.getIsAdEnable() == 1) {
                                OpenAppAds appLifecycleObserver = new OpenAppAds(MyApplication.getInstance());
                                ProcessLifecycleOwner.get().getLifecycle().addObserver(appLifecycleObserver);
                            }
                        }

                    };
            appOpenAd.setFullScreenContentCallback(callback);
            appOpenAd.show(this);
            isShowingAd = true;
        } else {
            if (UserHelper.getShowAppOpen() == 1 && UserHelper.getIsAdEnable() == 1) {
                OpenAppAds appLifecycleObserver = new OpenAppAds(MyApplication.getInstance());
                ProcessLifecycleOwner.get().getLifecycle().addObserver(appLifecycleObserver);
            }
        }

        hideSystemBars();

        binding.linStart.setOnClickListener(view1 -> {
            if (UserHelper.isPermissionGrant()){
                Intent mIntent = new Intent(ActStart.this, ActMain.class);
                startActivity(mIntent);
                finish();
            }else {
                permissonDialog();
            }
        });


        MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, ActStart.this, 1);
    }

    private void permissonDialog() {
        if (arePermissionDenied()) {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(1);
            dialog.setContentView(R.layout.dialog_permission);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            TextView tvAllow = dialog.findViewById(R.id.tvAllow);
            TextView tvCancel = dialog.findViewById(R.id.tvCancel);


            tvAllow.setOnClickListener(view -> {
                toMain();
            });
            tvCancel.setOnClickListener(view -> {
                if (dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } else {
            toMain();
        }

    }

    private void toMain() {
        Intent mIntent = new Intent(ActStart.this, ActMain.class);
        startActivity(mIntent);
        finish();
    }

    private boolean arePermissionDenied() {
        Common.APP_DIR = Environment.getExternalStorageDirectory().getPath() +
                File.separator + "StatusDownloader";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return getContentResolver().getPersistedUriPermissions().size() <= 0;
        }

        for (String permissions : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), permissions) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }

        return false;
    }
}