package com.errorhunt.whatsup.auto.screens;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.errorhunt.whatsup.auto.MyApplication;
import com.errorhunt.whatsup.auto.Utils.notifier.EventNotifier;
import com.errorhunt.whatsup.auto.Utils.notifier.EventState;
import com.errorhunt.whatsup.auto.Utils.notifier.IEventListener;
import com.errorhunt.whatsup.auto.Utils.notifier.NotifierFactory;
import com.errorhunt.whatsup.auto.databinding.ActSearchProfileBinding;


public class ActSearchProfile extends AppCompatActivity implements IEventListener {
    ActSearchProfileBinding binding;


    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            eventState = EventState.EVENT_PROCESSED;
            runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, ActSearchProfile.this, 1), 500));
        }

        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActSearchProfileBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        MyApplication.getInstance().loadBanner(binding.adViewBanner, ActSearchProfile.this);

        registerAdsListener();

        binding.toolbar.mToolBarThumb.setOnClickListener(v -> onBackPressed());
        binding.toolbar.mToolBarText.setText("Search Profile");


        binding.btnSearchProfile.setOnClickListener(v -> {
            if (isValidMobile(binding.etMobileNumber.getText().toString())) {
                Uri uri = Uri.parse("smsto:" + binding.etMobileNumber.getText().toString());
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, ""));
            } else {
                Toast.makeText(ActSearchProfile.this, "Enter valid Mobile Number..", Toast.LENGTH_SHORT).show();
            }

        });

        MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, ActSearchProfile.this, 1);
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public void backclickProfile(View view) {
        onBackPressed();
    }
}