package com.errorhunt.whatsup.auto.fragment;

import static android.Manifest.permission.CAMERA;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.errorhunt.whatsup.auto.MyApplication;
import com.errorhunt.whatsup.auto.R;
import com.errorhunt.whatsup.auto.Utils.UserHelper;
import com.errorhunt.whatsup.auto.Utils.notifier.EventNotifier;
import com.errorhunt.whatsup.auto.Utils.notifier.EventState;
import com.errorhunt.whatsup.auto.Utils.notifier.IEventListener;
import com.errorhunt.whatsup.auto.Utils.notifier.NotifierFactory;
import com.errorhunt.whatsup.auto.databinding.FragHomeBinding;
import com.errorhunt.whatsup.auto.qr_code_maker.ActQRcodeMaker;
import com.errorhunt.whatsup.auto.qr_code_reader.ActQRcodeReader;
import com.errorhunt.whatsup.auto.screens.ActSearchProfile;
import com.errorhunt.whatsup.auto.screens.ActTextRepeater;
import com.errorhunt.whatsup.auto.screens.ActWhatsAppWeb;
import com.errorhunt.whatsup.auto.screens.SettingActivity;
import com.errorhunt.whatsup.auto.whatapp_booster.WhatsCleanActivity;
import com.errorhunt.whatsup.auto.whatsapp_photos.ActWhatsappPhotos;
import com.errorhunt.whatsup.auto.whatsapp_saved_media.ActSaveMedia;
import com.errorhunt.whatsup.auto.whatsapp_video.ActWhatsappVideos;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FragHome extends BaseFragment implements View.OnClickListener, IEventListener {

    private static final String TEMPFILES = "TEMPORARIESFILESALL";
    public long free = 0;
    public long total = 0;
    FragHomeBinding binding;

    public static void takePermission(Activity activity, int request_code) {
        ActivityCompat.requestPermissions(activity,
                new String[]{CAMERA}, request_code);
    }

    public static boolean isPermissionGranted(Activity activity) {
        int camera = ContextCompat.checkSelfPermission(activity, CAMERA);
        return camera == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            eventState = EventState.EVENT_PROCESSED;
            activity.runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, activity, 1), 500));
            activity.runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder2, activity, 1), 500));
        }

        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragHomeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();


        ViewPagerAdapter adapter = new ViewPagerAdapter(requireActivity().getSupportFragmentManager());
        binding.vpPosters.setAdapter(adapter);
        binding.arcPi.setViewPager(binding.vpPosters);
        binding.vpPosters.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        binding.cardWhatsappStatusImage.setOnClickListener(this);
        binding.cardWhatsappStatusVideo.setOnClickListener(this);
        binding.cardWebScan.setOnClickListener(this);
        binding.cardSearchProfile.setOnClickListener(this);
        binding.cardRepeater.setOnClickListener(this);
        binding.cardQrGenerator.setOnClickListener(this);
        binding.cardQrCodeReader.setOnClickListener(this);
        binding.cardWhatsAppBooster.setOnClickListener(this);
        binding.ivDots.setOnClickListener(this);
        binding.cardSaveMedia.setOnClickListener(this);

        registerAdsListener();


        SharedPreferences sharedPreferences = activity.getSharedPreferences("phar_id", Context.MODE_PRIVATE);
        int memoryRange = sharedPreferences.getInt(TEMPFILES, (int) (Math.random() * 70) + 30);

        setProgressInAsync(binding.memoryPercantage, binding.memoryProgress, memoryRange, false);
        getMemorySize();
        parse();
        return view;
    }

    private void parse() {
        binding.freeRam.setText(calSize((double) this.free) + "");
        binding.ramTotal.setText(" / " + calSize((double) this.total));
    }

    private void getMemorySize() {
        Pattern compile = Pattern.compile("([a-zA-Z]+):\\s*(\\d+)");
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile("/proc/meminfo", "r");
            while (true) {
                CharSequence readLine = randomAccessFile.readLine();
                if (readLine != null) {
                    Matcher matcher = compile.matcher(readLine);
                    if (matcher.find()) {
                        String group = matcher.group(1);
                        String group2 = matcher.group(2);
                        if (group.equalsIgnoreCase("MemTotal")) {
                            this.total = Long.parseLong(group2);
                        } else if (group.equalsIgnoreCase("MemFree") || group.equalsIgnoreCase("SwapFree")) {
                            this.free = Long.parseLong(group2);
                        }
                    }
                } else {
                    randomAccessFile.close();
                    this.total *= 1 << 10;
                    this.free *= 1 << 10;
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String calSize(double d) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        double d2 = d / 1048576.0d;
        double d3 = d / 1.073741824E9d;
        double d4 = d / 1.099511627776E12d;
        if (d4 > 1.0d) {
            return decimalFormat.format(d4).concat(" TB");
        }
        if (d3 > 1.0d) {
            return decimalFormat.format(d3).concat(" GB");
        }
        if (d2 > 1.0d) {
            return decimalFormat.format(d2).concat(" MB");
        }
        return decimalFormat.format(d).concat(" KB");
    }

    public void setProgressInAsync(final TextView percantage, final ProgressBar progressBar, final int progress, final boolean justNow) {
        new Thread(() -> {
            if (justNow) {
                activity.runOnUiThread(() -> {
                    percantage.setText(progress + "MB");
                    progressBar.setProgress(progress);
                });
            } else {
                int currentRange = 0;

                while (currentRange < progress) {
                    currentRange++;

                    final int finalCurrentMomoryRange = currentRange;

                    activity.runOnUiThread(() -> {
                        percantage.setText(finalCurrentMomoryRange + "MB");
                        progressBar.setProgress(finalCurrentMomoryRange);
                    });

                    try {
                        Thread.sleep(65);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, activity, 1);
        MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder2, activity, 1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardWhatsappStatusImage:
                Intent mIntent = new Intent(activity, ActWhatsappPhotos.class);
                startActivity(mIntent);
                break;
            case R.id.ivDots:
                Intent intent = new Intent(activity, SettingActivity.class);
                startActivity(intent);
                break;
            case R.id.cardSaveMedia:
                Intent intentSave = new Intent(activity, ActSaveMedia.class);
                startActivity(intentSave);
                break;
            case R.id.cardWhatsappStatusVideo:
                Intent intent0 = new Intent(activity, ActWhatsappVideos.class);
                startActivity(intent0);
                break;
            case R.id.cardSearchProfile:
                Intent intent4 = new Intent(activity, ActSearchProfile.class);
                if (UserHelper.isNetworkConnected(activity)) {
                    MyApplication.getInstance().displayInterstitialAds(activity, intent4, false);
                } else {
                    startActivity(intent4);
                }
                break;

            case R.id.cardRepeater:
                Intent intentText = new Intent(activity, ActTextRepeater.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(activity, intentText, false);
                } else {
                    startActivity(intentText);
                }
                break;
            case R.id.cardWhatsAppBooster:
                Intent intentBooster = new Intent(activity, WhatsCleanActivity.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(activity, intentBooster, false);
                } else {
                    startActivity(intentBooster);
                }
                break;

            case R.id.cardWebScan:
                Intent intent21 = new Intent(requireActivity(), ActWhatsAppWeb.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(activity, intent21, false);
                } else {
                    startActivity(intent21);
                }
                break;


            case R.id.cardQrGenerator:
                Intent intent2 = new Intent(activity, ActQRcodeMaker.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(activity, intent2, false);
                } else {
                    startActivity(intent2);
                }
                break;
            case R.id.cardQrCodeReader:
                Intent intent3 = new Intent(activity, ActQRcodeReader.class);
                if (!isPermissionGranted(requireActivity())) {
                    takePermission(activity, 111);
                } else {
                    if (UserHelper.isNetworkConnected(requireActivity())) {
                        MyApplication.getInstance().displayInterstitialAds(activity, intent3, false);
                    } else {
                        startActivity(intent3);
                    }
                }
                break;
        }
    }


    public static class ViewPagerAdapter extends FragmentStatePagerAdapter {
        FragmentManager fragmentManager;

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            this.fragmentManager = fragmentManager;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            FragPostersView fragment = new FragPostersView();
            Bundle bundle = new Bundle();
            bundle.putSerializable("CurrentPosition", position);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return 5;
        }
    }
}