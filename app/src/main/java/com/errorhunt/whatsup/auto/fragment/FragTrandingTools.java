package com.errorhunt.whatsup.auto.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.errorhunt.whatsup.auto.databinding.FragTrandingToolsBinding;
import com.errorhunt.whatsup.auto.screens.ActFontToEmoji;
import com.errorhunt.whatsup.auto.screens.ActInfoWhatsApp;

import com.errorhunt.whatsup.auto.MyApplication;
import com.errorhunt.whatsup.auto.R;
import com.errorhunt.whatsup.auto.Utils.UserHelper;
import com.errorhunt.whatsup.auto.Utils.notifier.EventNotifier;
import com.errorhunt.whatsup.auto.Utils.notifier.EventState;
import com.errorhunt.whatsup.auto.Utils.notifier.IEventListener;
import com.errorhunt.whatsup.auto.Utils.notifier.NotifierFactory;
import com.errorhunt.whatsup.auto.ascii_face.ActAsciiFaceText;
import com.errorhunt.whatsup.auto.card_caption.ActCardCaption;
import com.errorhunt.whatsup.auto.reply.ActReply;
import com.errorhunt.whatsup.auto.shayri.ActShayri;
import com.errorhunt.whatsup.auto.sticker.ActSticker;

public class FragTrandingTools extends Fragment implements View.OnClickListener , IEventListener {

    FragTrandingToolsBinding binding;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, requireActivity(), 1), 500));
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder2, requireActivity(), 1), 500));
        }

        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }


    public static FragTrandingTools newInstance(String param1, String param2) {
        FragTrandingTools fragment = new FragTrandingTools();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragTrandingToolsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();

        binding.cardFontEmoji.setOnClickListener(this);
        binding.cardSticker.setOnClickListener(this);
        binding.cardReply.setOnClickListener(this);
        binding.cardCaption.setOnClickListener(this);
        binding.cardShayri.setOnClickListener(this);
        binding.cardAsciiFaceText.setOnClickListener(this);
        binding.cardInfoWhatsapp.setOnClickListener(this);

        registerAdsListener();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, requireActivity(), 1);
        MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder2, requireActivity(), 1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardFontEmoji:
                Intent intent0 = new Intent(requireActivity(), ActFontToEmoji.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent0, false);
                } else {
                    startActivity(intent0);
                }
                break;
            case R.id.cardSticker:
                Intent intent2 = new Intent(requireActivity(), ActSticker.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent2, false);
                } else {
                    startActivity(intent2);
                }
                break;
            case R.id.cardReply:
                Intent intent3 = new Intent(requireActivity(), ActReply.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent3, false);
                } else {
                    startActivity(intent3);
                }
                break;
            case R.id.cardCaption:
                Intent intent4 = new Intent(requireActivity(), ActCardCaption.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent4, false);
                } else {
                    startActivity(intent4);
                }
                break;
            case R.id.cardShayri:
                Intent intent5 = new Intent(requireActivity(), ActShayri.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent5, false);
                } else {
                    startActivity(intent5);
                }
                break;
            case R.id.cardAsciiFaceText:
                Intent intent1 = new Intent(requireActivity(), ActAsciiFaceText.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent1, false);
                } else {
                    startActivity(intent1);
                }
//                startActivity(intent1);
                break;
            case R.id.cardInfoWhatsapp:
                Intent intent6 = new Intent(requireActivity(), ActInfoWhatsApp.class);
                if (UserHelper.isNetworkConnected(requireActivity())) {
                    MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent6, false);
                } else {
                    startActivity(intent6);
                }
                break;
        }
    }
}