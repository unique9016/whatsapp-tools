package com.errorhunt.whatsup.auto.fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.errorhunt.whatsup.auto.MyApplication;
import com.errorhunt.whatsup.auto.R;
import com.errorhunt.whatsup.auto.Utils.notifier.EventNotifier;
import com.errorhunt.whatsup.auto.Utils.notifier.EventState;
import com.errorhunt.whatsup.auto.Utils.notifier.IEventListener;
import com.errorhunt.whatsup.auto.Utils.notifier.NotifierFactory;
import com.errorhunt.whatsup.auto.databinding.FragExtrasBinding;
import com.errorhunt.whatsup.auto.screens.ActAboutUs;


public class FragExtras extends Fragment implements View.OnClickListener, IEventListener {

    FragExtrasBinding binding;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, requireActivity(), 1), 500));
        }

        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragExtrasBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();

        binding.tvAboutUs.setOnClickListener(this);
        binding.tvMore.setOnClickListener(this);
        binding.tvContactUs.setOnClickListener(this);
        binding.ShareNow.setOnClickListener(this);
        binding.tvRateUs.setOnClickListener(this);

        registerAdsListener();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(binding.flAdplaceholder, requireActivity(), 1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAboutUs:
                startActivity(new Intent(requireActivity(), ActAboutUs.class));
                break;
            case R.id.tvMore:
                Intent browserIntent =
                        new Intent(Intent.ACTION_VIEW, Uri.parse("https://codecanyon.net/user/errorhunt"));
                startActivity(browserIntent);
                break;
            case R.id.tvContactUs:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"errorhunt3@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Your subject here...");
                intent.putExtra(Intent.EXTRA_TEXT, "Your message here...");
                startActivity(intent);
                /*try {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_APP_EMAIL);
                    this.startActivity(intent);
                } catch (android.content.ActivityNotFoundException e) {
                    Toast.makeText(requireActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }*/
                break;
            case R.id.ShareNow:
                ShareApp(requireActivity());
                break;
            case R.id.tvRateUs:
                RateApp(requireActivity());
                break;
        }
    }

    public void ShareApp(Context context) {
        final String appLink = "\nhttps://play.google.com/store/apps/details?id=" + context.getPackageName();
        Intent sendInt = new Intent(Intent.ACTION_SEND);
        sendInt.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
        sendInt.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.share_app_message) + appLink);
        sendInt.setType("text/plain");
        context.startActivity(Intent.createChooser(sendInt, "Share"));
    }

    public void RateApp(Context context) {
        final String appName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
        } catch (ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
        }
    }
}