package com.errorhunt.whatsup.auto.card_caption;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.errorhunt.whatsup.auto.databinding.FragQuotsViewBinding;


public class FragQuotView extends Fragment {

    FragQuotsViewBinding binding;
    int _position;
    String strQuots;
    String strAuthor;

    public FragQuotView() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragQuotsViewBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();

        _position = getArguments().getInt("CurrentPosition");

        strQuots = getArguments().getString("CurrentQuots");
        strAuthor = getArguments().getString("CurrentAuthor");

        binding.tvCaption.setText(strQuots);
        binding.tvAuthor.setText(strAuthor);
        return view;
    }
}