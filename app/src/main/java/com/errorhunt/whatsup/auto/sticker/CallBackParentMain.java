package com.errorhunt.whatsup.auto.sticker;

import java.util.List;


public interface CallBackParentMain<C> {

    List<C> getChildList();
    boolean isInitiallyExpanded();
}