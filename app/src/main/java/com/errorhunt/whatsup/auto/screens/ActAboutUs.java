package com.errorhunt.whatsup.auto.screens;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.errorhunt.whatsup.auto.databinding.ActAboutUsBinding;

public class ActAboutUs extends AppCompatActivity {
    ActAboutUsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActAboutUsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.toolbar.mToolBarThumb.setOnClickListener(v -> onBackPressed());
        binding.toolbar.mToolBarText.setText("About Us");
    }

    public void backAboutUs(View view) {
        onBackPressed();
    }
}