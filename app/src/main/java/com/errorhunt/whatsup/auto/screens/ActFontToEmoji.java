package com.errorhunt.whatsup.auto.screens;

import static com.errorhunt.whatsup.auto.screens.ActTextRepeater.hideSoftKeyboard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.errorhunt.whatsup.auto.MyApplication;
import com.errorhunt.whatsup.auto.Utils.ConstMethod;
import com.errorhunt.whatsup.auto.Utils.notifier.EventNotifier;
import com.errorhunt.whatsup.auto.Utils.notifier.EventState;
import com.errorhunt.whatsup.auto.Utils.notifier.IEventListener;
import com.errorhunt.whatsup.auto.Utils.notifier.NotifierFactory;
import com.errorhunt.whatsup.auto.databinding.ActFontToEmojiBinding;

import java.io.IOException;
import java.io.InputStream;

public class ActFontToEmoji extends AppCompatActivity implements IEventListener {
    ActFontToEmojiBinding binding;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            eventState = EventState.EVENT_PROCESSED;
            runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(binding.flAdplaceholder, ActFontToEmoji.this, 1), 500));
        }

        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActFontToEmojiBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        registerAdsListener();

        MyApplication.getInstance().loadBanner(binding.adViewBanner, ActFontToEmoji.this);

        binding.toolbar.mToolBarThumb.setOnClickListener(v -> onBackPressed());
        binding.toolbar.mToolBarText.setText("Font to Emoji");

        binding.btnTransform.setOnClickListener(v -> {
            hideSoftKeyboard(ActFontToEmoji.this);
            if (!TextUtils.isEmpty(binding.etText.getText().toString()) && !TextUtils.isEmpty(binding.etTextEmoji.getText().toString())) {
                char[] charArray = binding.etText.getText().toString().toCharArray();
                binding.etTextwithEmoji.setText(".\n");
                for (char c2 : charArray) {
                    if (c2 == '?') {
                        try {
                            InputStream open = getBaseContext().getAssets().open("ques.txt");
                            byte[] bArr = new byte[open.available()];
                            open.read(bArr);
                            open.close();
                            binding.etTextwithEmoji.append(new String(bArr).replaceAll("[*]", binding.etTextEmoji.getText().toString()) + "\n\n");
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    } else if (c2 == ((char) (c2 & '_')) || Character.isDigit(c2)) {
                        try {
                            InputStream open2 = getBaseContext().getAssets().open(c2 + ".txt");
                            byte[] bArr2 = new byte[open2.available()];
                            open2.read(bArr2);
                            open2.close();
                            binding.etTextwithEmoji.append(new String(bArr2).replaceAll("[*]", binding.etTextEmoji.getText().toString()) + "\n\n");
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        }
                    } else {
                        try {
                            InputStream open3 = getBaseContext().getAssets().open("low" + c2 + ".txt");
                            byte[] bArr3 = new byte[open3.available()];
                            open3.read(bArr3);
                            open3.close();
                            binding.etTextwithEmoji.append(new String(bArr3).replaceAll("[*]", binding.etTextEmoji.getText().toString()) + "\n\n");
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                    }
                }
            }
        });

        binding.btnCopy.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.etTextwithEmoji.getText().toString())) {
                ConstMethod.CopyToClipBoard(ActFontToEmoji.this, binding.etTextwithEmoji.getText().toString());
            } else {
                Toast.makeText(ActFontToEmoji.this, "Empty string.", Toast.LENGTH_SHORT).show();
            }
        });
        binding.btnClear.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.etTextwithEmoji.getText().toString())) {
                binding.etTextwithEmoji.getText().clear();
            } else {
                Toast.makeText(ActFontToEmoji.this, "Empty string.", Toast.LENGTH_SHORT).show();
            }
        });
        binding.btnShare.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.etTextwithEmoji.getText().toString())) {
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, binding.etTextwithEmoji.getText().toString());
                try {
                    startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ActFontToEmoji.this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(ActFontToEmoji.this, "Empty string.", Toast.LENGTH_SHORT).show();
            }
        });
        MyApplication.getInstance().loadNativeAds(binding.flAdplaceholder, ActFontToEmoji.this, 1);
    }


}