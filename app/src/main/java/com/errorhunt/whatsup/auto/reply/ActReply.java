package com.errorhunt.whatsup.auto.reply;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.errorhunt.whatsup.auto.databinding.ActReplyBinding;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.errorhunt.whatsup.auto.Utils.ConstMethod;

import com.errorhunt.whatsup.auto.MyApplication;
import com.errorhunt.whatsup.auto.R;
import com.errorhunt.whatsup.auto.Utils.SharedPrefsHelper;
import com.errorhunt.whatsup.auto.Utils.notifier.EventNotifier;
import com.errorhunt.whatsup.auto.Utils.notifier.EventState;
import com.errorhunt.whatsup.auto.Utils.notifier.IEventListener;
import com.errorhunt.whatsup.auto.Utils.notifier.NotifierFactory;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ActReply extends AppCompatActivity implements IEventListener {
    ActReplyBinding binding;
    RepeaterReply adapterReply;
    ArrayList<String> parentArrayList = new ArrayList<>();

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            eventState = EventState.EVENT_PROCESSED;
            runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(binding.flAdplaceholder, ActReply.this, 1), 500));
        }

        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActReplyBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        registerAdsListener();


        RelativeLayout adViewBanner = findViewById(R.id.adViewBanner);
        MyApplication.getInstance().loadBanner(adViewBanner, ActReply.this);

        binding.toolbar.mToolBarThumb.setOnClickListener(v -> onBackPressed());
        binding.toolbar.mToolBarText.setText("Replay");

        binding.replyRecycler.setLayoutManager(new LinearLayoutManager(ActReply.this));

        notifyItems();

        binding.addFloatingBtn.setOnClickListener(v -> {

            ViewDialog alert = new ViewDialog();
            alert.showDialog(ActReply.this);

        });

        MyApplication.getInstance().loadNativeAds(binding.flAdplaceholder, ActReply.this, 1);
    }
    public void notifyItems() {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();

        if (parentArrayList == null) {
            parentArrayList = new ArrayList<>();
        }

        parentArrayList.clear();

        parentArrayList.add("Hello...");
        parentArrayList.add("How are you ?");
        parentArrayList.add("I'm Good, What about you");
        parentArrayList.add("Violence. Violence. Violence. I don't like it!");
        parentArrayList.add("I'm From Surat");

        String strFromDatabase = SharedPrefsHelper.getInstance().getString("LOCAL_DATA", "[]");
        ArrayList<String> stringArrayList = gson.fromJson(strFromDatabase, type);

        for (int i = 0; i < stringArrayList.size(); i++) {
            parentArrayList.add(stringArrayList.get(i));
        }

        adapterReply = new RepeaterReply(ActReply.this, parentArrayList, new ReplyListener() {
            @Override
            public void onClickEdit(String s, int layoutrPosition) {
                ViewDialog alert = new ViewDialog();
                alert.showDialog(ActReply.this,s,layoutrPosition);
            }

            @Override
            public void onClickCopy(String s) {
                ConstMethod.CopyToClipBoard(ActReply.this,s);
            }

            @Override
            public void onClickShare(String s) {
                if (!TextUtils.isEmpty(s)) {
                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                    whatsappIntent.setType("text/plain");
                    whatsappIntent.setPackage("com.whatsapp");
                    whatsappIntent.putExtra(Intent.EXTRA_TEXT,s);
                    try {
                        startActivity(whatsappIntent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(ActReply.this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActReply.this, "Empty string.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.replyRecycler.setAdapter(adapterReply);
    }

    public class ViewDialog {

        public void showDialog(Activity activity){
            final Dialog dialog = new Dialog(activity);
            dialog.setContentView(R.layout.reply_dialogs);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            EditText etTextMsg = dialog.findViewById(R.id.etTextReplyMsg);
            Button dialogButton1 = dialog.findViewById(R.id.btnCancel);
            Button dialogButton2 = dialog.findViewById(R.id.btnSave);
            dialogButton1.setOnClickListener(v -> dialog.dismiss());
            dialogButton2.setOnClickListener(v -> {
                dialog.dismiss();
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<String>>() {
                }.getType();

                String strFromDatabase = SharedPrefsHelper.getInstance().getString("LOCAL_DATA", "[]");
                ArrayList<String> stringArrayList = gson.fromJson(strFromDatabase, type);

                if (!TextUtils.isEmpty(etTextMsg.getText().toString())) {
                    stringArrayList.add(etTextMsg.getText().toString());
                    String saveData = gson.toJson(stringArrayList);
                    SharedPrefsHelper.getInstance().setString("LOCAL_DATA", saveData);

                    if (adapterReply != null) {
                        notifyItems();
                    }
                }else {
                    Toast.makeText(activity, "Please enter message..", Toast.LENGTH_SHORT).show();
                    return;
                }
            });

            dialog.show();

        }

        public void showDialog(ActReply replyActivity, String s, int layoutrPosition) {
            final Dialog dialog = new Dialog(replyActivity);
            dialog.setContentView(R.layout.reply_dialogs);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            EditText etTextMsg = dialog.findViewById(R.id.etTextReplyMsg);
            etTextMsg.setText(s);
            Button dialogButton1 = dialog.findViewById(R.id.btnCancel);
            Button dialogButton2 = dialog.findViewById(R.id.btnSave);
            dialogButton1.setOnClickListener(v -> dialog.dismiss());
            dialogButton2.setOnClickListener(v -> {
                dialog.dismiss();
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<String>>() {
                }.getType();

                String strFromDatabase = SharedPrefsHelper.getInstance().getString("LOCAL_DATA", "[]");
                ArrayList<String> stringArrayList = gson.fromJson(strFromDatabase, type);


                if (!TextUtils.isEmpty(etTextMsg.getText().toString())) {
                    stringArrayList.remove(layoutrPosition-5);
                    stringArrayList.add(layoutrPosition-5,etTextMsg.getText().toString());
//                        stringArrayList.add(etTextMsg.getText().toString());
                    String saveData = gson.toJson(stringArrayList);
                    SharedPrefsHelper.getInstance().setString("LOCAL_DATA", saveData);

                    if (adapterReply != null) {
                        notifyItems();
                    }
                }else {
                    Toast.makeText(replyActivity, "Please enter message..", Toast.LENGTH_SHORT).show();
                    return;
                }
            });

            dialog.show();
        }
    }

    public void backReply(View view) {
        onBackPressed();
    }
}