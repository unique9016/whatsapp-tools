package com.errorhunt.whatsup.auto.screens;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import com.errorhunt.whatsup.auto.R;
import com.errorhunt.whatsup.auto.Utils.Utility;
import com.errorhunt.whatsup.auto.databinding.ActThankYouBinding;

public class ActivityThankYou extends BaseAct {
    ActThankYouBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActThankYouBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        hideSystemBars();

        Bitmap bitmapLocal1 = Utility.decodeSampledBitmapFromResource(getResources(), R.mipmap.ic_launcher, 500, 500);
        binding.ivThankyou.setImageBitmap(bitmapLocal1);

        Bitmap bitmapLocal2 = Utility.decodeSampledBitmapFromResource(getResources(), R.drawable.e_tnx, 500, 500);
        binding.ivThanksTxt.setImageBitmap(bitmapLocal2);

        new Handler(Looper.getMainLooper()).postDelayed(this::finish, 1500);
    }
}