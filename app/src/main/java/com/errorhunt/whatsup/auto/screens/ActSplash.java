package com.errorhunt.whatsup.auto.screens;

import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.errorhunt.whatsup.auto.databinding.ActSplashBinding;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.errorhunt.whatsup.auto.MyApplication;
import com.errorhunt.whatsup.auto.R;
import com.errorhunt.whatsup.auto.Utils.Logger;
import com.errorhunt.whatsup.auto.Utils.UserHelper;
import com.errorhunt.whatsup.auto.Utils.Utility;
import com.errorhunt.whatsup.auto.model.AppData;

public class ActSplash extends BaseAct {
    ActSplashBinding binding;
    private static final int RC_APP_UPDATE = 11;

    private boolean isAppOpenAdLoad = false;
    private AppUpdateManager mAppUpdateManager;

    InstallStateUpdatedListener mInstallUpdated = new
            InstallStateUpdatedListener() {
                @Override
                public void onStateUpdate(InstallState state) {
                    if (state.installStatus() == InstallStatus.DOWNLOADED) {
                        snackbarUpdateApp();
                    } else if (state.installStatus() == InstallStatus.INSTALLED) {
                        if (mAppUpdateManager != null) {
                            mAppUpdateManager.unregisterListener(mInstallUpdated);
                        }
                    }
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActSplashBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        hideSystemBars();

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);


        binding.ivAppIcon.setImageBitmap(Utility.decodeSampledBitmapFromResource(getResources(), R.drawable.e_splash_logo, 500, 500));
        binding.ivHrzBanner.setImageBitmap(Utility.decodeSampledBitmapFromResource(getResources(), R.drawable.e_hrz_banner, 500, 500));

        UserHelper.setUserInSplashIntro(true);
        UserHelper.ads_per_session = 0;
        UserHelper.setReviewCount(0);

        doNext("{\"banner_id\":\"ca-app-pub-9621145161338476/8033284525\",\"interstitial_id\":\"ca-app-pub-9621145161338476/9154794505\",\"native_id\":\"ca-app-pub-9621145161338476/8963222811\",\"open_app_id\":\"ca-app-pub-9621145161338476/3710896137\",\"is_show_ads\":1,\"show_banner\":1,\"show_interstitial\":1,\"show_interstitial_exit\":1,\"interstitial_count\":3,\"show_native\":1,\"show_open_app\":1,\"show_open_app_splash\":1,\"ads_per_seassion\":10,\"relocation\":\"\",\"adtype\":\"admob\",\"reviewPopUpCount\":3}");

        inAppUpdate();

    }

    @Override
    public void onDestroy() {
        mAppUpdateManager.unregisterListener(mInstallUpdated);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserHelper.setUserInSplashIntro(true);
        try {
            mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(result -> {
                if (result.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                    try {
                        mAppUpdateManager.startUpdateFlowForResult(result, IMMEDIATE, ActSplash.this, RC_APP_UPDATE);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            });
            mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(appUpdateInfo -> {
                if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                    snackbarUpdateApp();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void snackbarUpdateApp() {
    }

    private void inAppUpdate() {

        mAppUpdateManager = AppUpdateManagerFactory.create(this);

        mAppUpdateManager.registerListener(mInstallUpdated);

        mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)) {

                try {
                    mAppUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo, IMMEDIATE, ActSplash.this, RC_APP_UPDATE);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }

            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                snackbarUpdateApp();
            }
        });
    }


    public void doNext(String response) {
        AppData appData = parseAppUserListModel(response);
        setAppData(appData);

        if (UserHelper.getIsAdEnable() == 1 && UserHelper.isNetworkConnected(ActSplash.this)) {
            if (UserHelper.getShowInterstitial() == 1) {
                MyApplication.getInstance().loadInterstitialAd(ActSplash.this);
            }
            if (UserHelper.getShowNative() == 1) {
                MyApplication.getInstance().loadAdmobNativeAd(0, ActSplash.this);
            }
            if (UserHelper.getShowAppOpen() == 1) {

                MyApplication.getInstance().loadOpenAppAdsOnSplash(isLoaded -> {
                    if (isLoaded && !isFinishing() && !isDestroyed()) {
                        Log.e("Ads Next", "From OpenApp Load");
                        isAppOpenAdLoad = true;
                        toHome();
                    }
                }, ActSplash.this);


                new Handler(Looper.myLooper()).postDelayed(() -> {
                    if (!isAppOpenAdLoad) {
                        Log.e("Ads Next ", "From OpenApp Time Out");
                        toHome();
                    }
                }, UserHelper.getSplashScreenWaitCount() * 1000L);

            } else {
                toHome();
                Log.e(MyApplication.ADMOB_TAG, "OpenApps Ads ===> Disable");
            }

        } else {
            toHome();
        }

    }


    public AppData parseAppUserListModel(String jsonObject) {
        try {
            Gson gson = new Gson();
            TypeToken<AppData> token = new TypeToken<AppData>() {
            };
            return gson.fromJson(jsonObject, token.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setAppData(AppData appData) {

        try {
            UserHelper.setBannerAd(appData.getBannerid());
            UserHelper.setShowBanner(appData.getBanner());

            UserHelper.setInterstitialAd(appData.getInterstitialid());
            UserHelper.setShowInterstitial(appData.getInterstitial());
            UserHelper.setInterstitialAdsClick(appData.getAds_per_click());

            UserHelper.setNativeAd(String.valueOf(appData.getNativeid()));
            UserHelper.setShowNative(appData.getNative());

            UserHelper.setAppOpenAd(String.valueOf(appData.getOpenadid()));
            UserHelper.setShowAppOpen(appData.getOpenad());
            UserHelper.setOpenAdsSplash(appData.getIs_splash_on());

            UserHelper.setAdType(String.valueOf(appData.getAdtype()));
            UserHelper.setIsAdEnable(appData.getIsAdEnable());
            UserHelper.setExitAdEnable(appData.getExit_ad_enable());
            UserHelper.setAdsPerSession(appData.getAds_per_session());
            UserHelper.setSplashScreenWaitCount(appData.getSplash_time());

            UserHelper.setLogo_Link(appData.getLogoLink());
            UserHelper.setLogo_Link_2(appData.getLogoLink_2());
            UserHelper.setLeagues_Link(appData.getLeaguesLink());
            UserHelper.setGet_Player(appData.getGetPlayer());
            UserHelper.setGet_Match(appData.getGetMatch());
            UserHelper.setGetLeague_Name(appData.getGetLeagueName());
            UserHelper.setIsShowHomeBanner(appData.getIsShowHomeBanner());
            UserHelper.setBannerDataFlag(appData.getBannerDataFlag());
            UserHelper.setReviewPopupCount(appData.getReviewPopUpCount());

        } catch (Exception e) {
            Logger.AppLog("Ads:: Exception", e.getMessage());
        }
    }

    private void toHome() {
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            startActivity(new Intent(ActSplash.this, ActStart.class));
            finish();
        }, 3000);

    }
}
