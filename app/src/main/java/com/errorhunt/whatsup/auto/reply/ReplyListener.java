package com.errorhunt.whatsup.auto.reply;

public interface ReplyListener {
    void onClickEdit(String s,int layoutrPosition);
    void onClickCopy(String s);
    void onClickShare(String s);
}
