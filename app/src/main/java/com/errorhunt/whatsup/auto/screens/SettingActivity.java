package com.errorhunt.whatsup.auto.screens;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.errorhunt.whatsup.auto.R;
import com.errorhunt.whatsup.auto.databinding.ActivitySettingBinding;
import com.errorhunt.whatsup.auto.fragment.FragExtras;

public class SettingActivity extends AppCompatActivity {

    ActivitySettingBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySettingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new FragExtras()).commit();

        binding.toolbar.mToolBarThumb.setOnClickListener(v -> onBackPressed());

    }
}